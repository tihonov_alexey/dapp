// var module = require('./index');
// var http = require('http');/* получение модуля http, который необходим для создания сервера */
// var fs = require('fs');/*

// var printSomething = function() {
//     console.log("Просто текст");
// };
//
// module.printSomething;
// console.log(module.some_value);
//
// /*  создание нового сервера для прослушивания входящих подключений и обработки запросов */
// var server = http.createServer(function(req,res){
//     if (req.url === '/index' || req.url === '/') {
//         res.writeHead(200, {'Content-Type' : 'text/html; charset=utf-8'});
//         fs.createReadStream(__dirname + '/index.html', 'utf-8').pipe(res);
//     } else {
//         res.writeHead(404, {'Content-Type' : 'text/html; charset=utf-8'});
//         fs.createReadStream(__dirname + '/404.html', 'utf-8').pipe(res);
//     }
// });
//
// server.listen(3000, "127.0.0.1",function(){
//     console.log("Сервер начал прослушивание запросов на порту 3000");
// });


var express = require('express'); /* получение модуля express */

var app = express();

app.set('view engine', 'ejs');

appp.use('public', express.static('public'));

app.get('/', function (req, res) {
    res.send('This is home');
});

app.get('/news/:name', function (req, res) {
    res.render('news', {newsName: req.params.name});
});

app.listen(3000);