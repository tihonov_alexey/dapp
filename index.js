function test() {
    console.log("Привет!");
}

test();

function call(func) {
    func();
}

var multiply = function(x, y) {
    return `$(x) * $(y) = $(x * y)`;
};

var some_value = 451;

module.exports = {
    call: call,
    multiply: multiply,
    some_value: some_value
};